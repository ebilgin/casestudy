package com.casestudy.enzblgn.application.appstate

import android.os.Bundle
import android.os.Parcelable
import java.util.*

open class AppStateImpl : AppState {

    private val values: Bundle = Bundle()

    override fun <T : Parcelable> getObject(key: String): T? {
        return values.getParcelable(key)
    }

    override fun <T : Parcelable> getArrayList(key: String): ArrayList<T>? {
        return values.getParcelableArrayList(key)
    }

    override fun getStringArrayList(key: String): ArrayList<String>? {
        return values.getStringArrayList(key)
    }

    override fun getIntegerArrayList(key: String): ArrayList<Int>? {
        return values.getIntegerArrayList(key)
    }

    override fun getString(key: String, defaultValue: String?): String? {
        return values.getString(key, defaultValue)
    }

    override fun getInt(key: String, defaultValue: Int): Int {
        return values.getInt(key, defaultValue)
    }

    override fun getLong(key: String, defaultValue: Long): Long {
        return values.getLong(key, defaultValue)
    }

    override fun getDouble(key: String, defaultValue: Double): Double {
        return values.getDouble(key, defaultValue)
    }

    override fun getFloat(key: String, defaultValue: Float): Float {
        return values.getFloat(key, defaultValue)
    }

    override fun getBoolean(key: String, defaultValue: Boolean): Boolean {
        return values.getBoolean(key, defaultValue)
    }

    override fun getDate(key: String, defaultValue: Date): Date {
        val dateValueAsLong = getLong(key, 0)
        return Date(dateValueAsLong)
    }

    override fun <T : Parcelable> putObject(key: String, value: T) {
        values.putParcelable(key, value)
    }

    override fun putArrayList(key: String, value: ArrayList<out Parcelable?>) {
        values.putParcelableArrayList(key, value)
    }

    override fun putStingArrayList(key: String, value: ArrayList<String>) {
        values.putStringArrayList(key, value)
    }

    override fun putIntArrayList(key: String, value: ArrayList<Int>) {
        values.putIntegerArrayList(key, value)
    }

    override fun putString(key: String, value: String?) {
        values.putString(key, value)
    }

    override fun putInt(key: String, value: Int) {
        values.putInt(key, value)
    }

    override fun putLong(key: String, value: Long) {
        values.putLong(key, value)
    }

    override fun putDouble(key: String, value: Double) {
        values.putDouble(key, value)
    }

    override fun putFloat(key: String, value: Float) {
        values.putFloat(key, value)
    }

    override fun putBoolean(key: String, value: Boolean) {
        values.putBoolean(key, value)
    }

    override fun putDate(key: String, value: Date) {
        putLong(key, value.time)
    }

    override fun remove(key: String) {
        values.remove(key)
    }

    override fun clear() {
        values.clear()
    }
}