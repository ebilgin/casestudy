package com.casestudy.enzblgn.application.appstate

import android.os.Parcelable
import java.util.*

interface AppState {

    fun <T : Parcelable> getObject(key: String): T?

    fun <T : Parcelable> getArrayList(key: String): ArrayList<T>?

    fun getStringArrayList(key: String): ArrayList<String>?

    fun getIntegerArrayList(key: String): ArrayList<Int>?

    fun getString(key: String, defaultValue: String? = null): String?

    fun getInt(key: String, defaultValue: Int = 0): Int

    fun getLong(key: String, defaultValue: Long = 0L): Long

    fun getDouble(key: String, defaultValue: Double = 0.0): Double

    fun getFloat(key: String, defaultValue: Float = 0f): Float

    fun getBoolean(key: String, defaultValue: Boolean = false): Boolean

    fun getDate(key: String, defaultValue: Date = Date(0L)): Date

    fun <T : Parcelable> putObject(key: String, value: T)

    fun putArrayList(key: String, value: ArrayList<out Parcelable?>)

    fun putStingArrayList(key: String, value: ArrayList<String>)

    fun putIntArrayList(key: String, value: ArrayList<Int>)

    fun putString(key: String, value: String? = null)

    fun putInt(key: String, value: Int = 0)

    fun putLong(key: String, value: Long = 0L)

    fun putDouble(key: String, value: Double = 0.0)

    fun putFloat(key: String, value: Float = 0f)

    fun putBoolean(key: String, value: Boolean = false)

    fun putDate(key: String, value: Date = Date(0L))

    fun remove(key: String)

    fun clear()
}