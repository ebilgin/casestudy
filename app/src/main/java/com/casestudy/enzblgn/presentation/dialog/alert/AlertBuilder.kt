package com.casestudy.enzblgn.presentation.dialog.alert

import android.graphics.drawable.Drawable
import android.view.View
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

class AlertBuilder {

    @StringRes
    var messageResID: Int = 0

    var message: String? = null
    var title: String? = null
    var icon: Drawable? = null

    @DrawableRes
    var iconResID: Int = 0

    var positiveButtonTitle: String? = null
    var positiveButtonClickListener: ClickHandler? = null
    var negativeButtonTitle: String? = null
    var negativeButtonClickListener: ClickHandler? = null
    var dismissCallback: ClickHandler? = null

    var customView: View? = null
    var cancelable: Boolean = false
}


typealias BuilderHandler = AlertBuilder.() -> Unit
typealias ClickHandler = () -> Unit