package com.casestudy.enzblgn.presentation.dialog.alert

interface AlertPresenter {

    fun showAlert(builder: BuilderHandler)
}
