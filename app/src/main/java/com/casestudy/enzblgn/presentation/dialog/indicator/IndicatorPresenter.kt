package com.casestudy.enzblgn.presentation.dialog.indicator


interface IndicatorPresenter {

    fun show()

    fun hide()
}