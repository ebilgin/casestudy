package com.casestudy.enzblgn.presentation.data


interface ResourceError {
    fun code(): Int
}