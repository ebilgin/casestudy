package com.casestudy.enzblgn.presentation.extensions

import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.fragment.app.Fragment


fun Fragment.hideKeyboard() {
    val insetsController = ViewCompat.getWindowInsetsController(requireActivity().window.decorView)
    insetsController?.hide(WindowInsetsCompat.Type.ime())
}


