package com.casestudy.enzblgn.presentation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<T>(
    private val itemLayoutRes: Int,
    var items: List<T>,
) : RecyclerView.Adapter<BaseViewHolder<T>>() {

    var callback: OnItemClickListener<T>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<T> {
        val view = LayoutInflater.from(parent.context).inflate(itemLayoutRes, parent, false)
        return BaseViewHolderImp(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        return itemLayoutRes
    }

    override fun onBindViewHolder(holder: BaseViewHolder<T>, position: Int) {
        holder.bind(items[position], position)
    }

    inner class BaseViewHolderImp(itemView: View) : BaseViewHolder<T>(itemView) {
        override fun bind(item: T, position: Int) {
            this@BaseAdapter.bind(itemView, item, position, this)
            callback?.let { it ->
                itemView.setOnClickListener { _ ->
                    it.onItemClick(item)
                }
            }
        }
    }

    abstract fun bind(itemView: View, item: T, position: Int, viewHolder: BaseViewHolderImp)

    open fun setOnItemClickListener(callback: OnItemClickListener<T>) {
        this.callback = callback
    }

    interface OnItemClickListener<T> {
        fun onItemClick(item: T)
    }
}

