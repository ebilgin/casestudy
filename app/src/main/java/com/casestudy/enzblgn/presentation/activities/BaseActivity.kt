package com.casestudy.enzblgn.presentation.activities

import android.os.Bundle
import androidx.annotation.ContentView
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import com.casestudy.enzblgn.common.extensions.*
import com.casestudy.enzblgn.presentation.data.*
import com.casestudy.enzblgn.presentation.dialog.alert.AlertPresenter
import com.casestudy.enzblgn.presentation.dialog.indicator.IndicatorPresenter
import javax.inject.Inject


abstract class BaseActivity : AppCompatActivity {

    constructor() : super()

    @ContentView
    constructor(@LayoutRes layoutResId: Int) : super(layoutResId)

    @Inject
    lateinit var alertPresenter: AlertPresenter

    @Inject
    lateinit var indicatorPresenter: IndicatorPresenter

    protected abstract val binding: ViewBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setBindingView()
    }

    open fun setBindingView() {
        val view = binding.root
        setContentView(view)
    }

    open fun <T> handleResource(
        resource: Resource<T>,
        successHandler: ResourceSuccessHandler<T>? = null,
        errorHandler: ResourceErrorHandler<T>? = null,
        errorActionHandler: ((error: ResourceError) -> Boolean)? = null,
        loadingHandler: ResourceLoadingHandler<T>? = null,
    ) {
        when (resource) {
            is Resource.Loading -> {
                if (loadingHandler == null) {
                    indicatorPresenter.show()
                } else {
                    loadingHandler(resource)
                }
            }
            is Resource.Success -> {
                if (successHandler == null) {
                    indicatorPresenter.hide()
                } else {
                    successHandler(resource)
                }
            }
            is Resource.Error -> {
                indicatorPresenter.hide()
                errorHandler?.let { handler ->
                    // handle custom error types
                    val isCustomErrorHandlerActive = handler(resource)
                    // skip default error handler
                    if (isCustomErrorHandlerActive) {
                        return
                    }
                }
                handleDefaultError(resource, errorActionHandler)
            }
        }
    }

    open fun <T> handleDefaultError(
        resource: Resource.Error<T>,
        errorActionHandler: ((error: ResourceError) -> Boolean)? = null,
    ) {
        val errorMessage = getString(resource.messageResID)
        alertPresenter.showAlert {
            message = errorMessage
            positiveButtonTitle = getString(android.R.string.ok)
            positiveButtonClickListener = clickHandler@{
                if (errorActionHandler != null && errorActionHandler(resource.error)) {
                    return@clickHandler
                }
                handleErrorAction(resource.error)
            }
        }
    }

    open fun handleErrorAction(error: ResourceError) {
        finish()
    }
}