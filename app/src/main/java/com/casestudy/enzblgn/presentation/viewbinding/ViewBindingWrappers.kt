package com.casestudy.enzblgn.presentation.viewbinding

import android.view.LayoutInflater
import android.view.View
import androidx.annotation.MainThread
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding

/**
 * wrapper above [FragmentViewBindingProperty]
 */
@MainThread
fun <T : ViewBinding> Fragment.fragmentViewBinding(viewBinder: (View) -> T): FragmentViewBindingProperty<T> =
    FragmentViewBindingProperty(viewBinder)

/**
 * wrapper above [ActivityViewBindingProperty]
 */
@MainThread
fun <T : ViewBinding> AppCompatActivity.activityViewBinding(bindingInitializer: (LayoutInflater) -> T): ActivityViewBindingProperty<T> =
    ActivityViewBindingProperty(bindingInitializer)