package com.casestudy.enzblgn.presentation.dialog.alert


import android.content.Context
import android.content.DialogInterface
import androidx.activity.ComponentActivity
import androidx.lifecycle.Lifecycle
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.casestudy.enzblgn.presentation.dialog.DialogDismissLifecycleObserver


open class DefaultAlertPresenter constructor(private val context: Context) : AlertPresenter {

    open var isShowing: Boolean = false

    override fun showAlert(builder: BuilderHandler) {
        if (!isShowing && isActivityRunning()) {
            isShowing = !isShowing
            val alertBuilder = AlertBuilder()
            builder(alertBuilder)
            createAlertDialog(alertBuilder)
        }
    }

    open fun createAlertDialog(
        builder: AlertBuilder,
    ) {
        val dialog = MaterialAlertDialogBuilder(context).run {
            setCancelable(builder.cancelable)
            setMessage(builder.message)
            setTitle(builder.title)
            setIcon(builder.icon)
            setOnDismissListener {
                builder.dismissCallback?.let { callback ->
                    callback()
                }
                isShowing = false
            }
            if (!builder.positiveButtonTitle.isNullOrEmpty()) {
                setPositiveButton(
                    builder.positiveButtonTitle
                ) { _, _ ->
                    builder.positiveButtonClickListener?.let { it() }
                }
            }

            if (!builder.negativeButtonTitle.isNullOrEmpty()) {
                setNegativeButton(
                    builder.negativeButtonTitle
                ) { _, _ ->
                    builder.negativeButtonClickListener?.let { it() }
                }
            }

            if (builder.customView != null) {
                setView(builder.customView)
            }

            if (builder.messageResID != 0) {
                setMessage(builder.messageResID)
            }

            if (builder.iconResID != 0) {
                setIcon(builder.iconResID)
            }
            create()
        }

        (context as ComponentActivity).lifecycle.addObserver(DialogDismissLifecycleObserver(dialog))
        dialog.show()

        dialog.getButton(DialogInterface.BUTTON_POSITIVE)?.apply {
            requestFocus()
            clearFocus()
        }
    }

    open fun isActivityRunning(): Boolean {
        return (context as ComponentActivity).lifecycle.currentState != Lifecycle.State.DESTROYED
    }
}