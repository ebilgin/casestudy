package com.casestudy.enzblgn.presentation.dialog

import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent

class DialogDismissLifecycleObserver(private var dialog: AlertDialog?) : LifecycleObserver {

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun destroy() {
        dialog?.dismiss()
        dialog = null
    }
}