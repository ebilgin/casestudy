package com.casestudy.enzblgn.presentation.fragments

import androidx.annotation.ContentView
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.casestudy.enzblgn.common.extensions.*
import com.casestudy.enzblgn.presentation.activities.BaseActivity
import com.casestudy.enzblgn.presentation.data.*
import com.casestudy.enzblgn.presentation.dialog.alert.AlertPresenter
import com.casestudy.enzblgn.presentation.dialog.indicator.IndicatorPresenter
import javax.inject.Inject

abstract class BaseFragment : Fragment {

    constructor() : super()

    @ContentView
    constructor(@LayoutRes layoutResId: Int) : super(layoutResId)

    @Inject
    lateinit var alertPresenter: AlertPresenter

    @Inject
    lateinit var indicatorPresenter: IndicatorPresenter

    protected abstract val binding: ViewBinding

    open fun <T> handleResource(
        resource: Resource<T>,
        successHandler: ResourceSuccessHandler<T>? = null,
        errorHandler: ResourceErrorHandler<T>? = null,
        loadingHandler: ResourceLoadingHandler<T>? = null,
    ) {
        (requireActivity() as BaseActivity).handleResource(
            resource,
            successHandler = successHandler,
            errorHandler = errorHandler,
            errorActionHandler = {
                handleErrorAction(it)
            },
            loadingHandler = loadingHandler
        )
    }

    open fun handleErrorAction(error: ResourceError): Boolean {
        return false
    }
}