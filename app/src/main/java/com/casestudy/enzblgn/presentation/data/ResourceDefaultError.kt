package com.casestudy.enzblgn.presentation.data


enum class ResourceDefaultError(private val errorCode: Int) : ResourceError {

    // error types
    GENERAL_ERROR(1001),
    HTTP_ERROR(1002),
    TIMEOUT_ERROR(1003),
    NOT_CONNECTION_ERROR(1004),
    MAPPING_ERROR(1006);

    override fun code(): Int {
        return this.errorCode
    }
}