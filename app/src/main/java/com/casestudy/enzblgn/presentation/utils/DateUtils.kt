package com.casestudy.enzblgn.presentation.utils

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    fun getDate(timestamp: Long): String {
        val calendar = Calendar.getInstance(Locale.ENGLISH)
        calendar.timeInMillis = timestamp * 1000L
        return formatDate(calendar.time, "MM/dd/yyyy, EEE").toString()
    }

    fun formatDate(date: Date, type: String): String? {
        val simpleDateFormat = SimpleDateFormat(type, Locale.getDefault())
        return simpleDateFormat.format(date)
    }
}
