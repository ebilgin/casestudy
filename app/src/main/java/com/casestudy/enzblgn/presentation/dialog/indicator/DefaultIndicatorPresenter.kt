package com.casestudy.enzblgn.presentation.dialog.indicator

import android.content.Context
import androidx.activity.ComponentActivity
import androidx.annotation.LayoutRes
import androidx.annotation.StyleRes
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Lifecycle
import com.casestudy.enzblgn.R
import com.casestudy.enzblgn.presentation.dialog.DialogDismissLifecycleObserver


open class DefaultIndicatorPresenter @JvmOverloads constructor(
    private val context: Context,
    @StyleRes private val themeResId: Int = 0,
    @LayoutRes private var indicatorViewResId: Int = R.layout.layout_loading_progress
) : IndicatorPresenter {

    private var dialog: AlertDialog? = null

    init {
        // default loading progress
        initLoadingProgress()
    }

    private fun initLoadingProgress() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(context, themeResId)
        builder.setCancelable(false)
        // loading view
        builder.setView(indicatorViewResId)
        this.dialog = builder.create()
        // set background
        dialog?.window?.setBackgroundDrawableResource(android.R.color.transparent)
        // add lifecycle
        (context as ComponentActivity).lifecycle.addObserver(DialogDismissLifecycleObserver(dialog))
    }

    override fun show() {
        if (dialog == null || !isActivityRunning()) {
            return
        }
        dialog?.let { alertDialog ->
            if (!alertDialog.isShowing) {
                alertDialog.show()
            }
        }
    }

    override fun hide() {
        if (dialog == null || !isActivityRunning()) {
            return
        }
        dialog?.let { alertDialog ->
            if (alertDialog.isShowing) {
                alertDialog.dismiss()
            }
        }
    }

    open fun isActivityRunning(): Boolean {
        return (context as ComponentActivity).lifecycle.currentState != Lifecycle.State.DESTROYED
    }
}