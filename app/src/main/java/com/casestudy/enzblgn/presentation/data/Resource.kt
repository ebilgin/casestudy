package com.casestudy.enzblgn.presentation.data

import com.casestudy.enzblgn.R


sealed class Resource<out T> {

    data class Success<T>(val data: T) : Resource<T>()

    data class Loading<T>(val data: T? = null, val loadingMessageResId: Int = 0) : Resource<T>()

    data class Error<T>(
        val data: T? = null,
        val messageResID: Int = R.string.common_technical_error_message,
        val error: ResourceError = ResourceDefaultError.GENERAL_ERROR
    ) : Resource<T>()
}
