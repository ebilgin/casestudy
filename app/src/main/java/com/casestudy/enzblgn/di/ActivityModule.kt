package com.casestudy.enzblgn.di

import android.content.Context
import com.casestudy.enzblgn.presentation.dialog.alert.AlertPresenter
import com.casestudy.enzblgn.presentation.dialog.alert.DefaultAlertPresenter
import com.casestudy.enzblgn.presentation.dialog.indicator.DefaultIndicatorPresenter
import com.casestudy.enzblgn.presentation.dialog.indicator.IndicatorPresenter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.qualifiers.ActivityContext
import dagger.hilt.android.scopes.ActivityScoped

@Module
@InstallIn(ActivityComponent::class)
class ActivityModule {

    @Provides
    @ActivityScoped
    fun provideAlertPresenter(@ActivityContext context: Context): AlertPresenter =
        DefaultAlertPresenter(context)


    @Provides
    @ActivityScoped
    fun provideIndicatorPresenter(@ActivityContext context: Context): IndicatorPresenter =
        DefaultIndicatorPresenter(context = context)
}