package com.casestudy.enzblgn.business.model


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import java.util.*

@Parcelize
data class MeditationModel(
    val content: String,
    val image: ImageModel,
    val releaseDate: String,
    val subtitle: String,
    val title: String
): Parcelable