package com.casestudy.enzblgn.business.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import com.google.gson.annotations.SerializedName

@Parcelize
data class ImageData(
    @SerializedName("large")
    val large: String,
    @SerializedName("small")
    val small: String
) : Parcelable