package com.casestudy.enzblgn.business.data


import com.google.gson.annotations.SerializedName

data class ResponseData(
    @SerializedName("isBannerEnabled")
    val isBannerEnabled: Boolean,
    @SerializedName("meditations")
    val meditations: List<MeditationData>,
    @SerializedName("stories")
    val stories: List<StoryData>
)