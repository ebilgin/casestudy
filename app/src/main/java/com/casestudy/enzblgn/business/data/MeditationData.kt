package com.casestudy.enzblgn.business.data


import com.google.gson.annotations.SerializedName

data class MeditationData(
    @SerializedName("content")
    val content: String,
    @SerializedName("image")
    val image: ImageData,
    @SerializedName("releaseDate")
    val releaseDate: Long,
    @SerializedName("subtitle")
    val subtitle: String,
    @SerializedName("title")
    val title: String
)