package com.casestudy.enzblgn.business.api

import com.casestudy.enzblgn.business.data.ResponseData
import com.casestudy.enzblgn.business.model.ResponseDataModel
import retrofit2.Response
import retrofit2.http.GET

interface ApiInterface {
    @GET("files/MobileInterviewProjectData.json")
    suspend fun getResponse(): Response<ResponseData>
}