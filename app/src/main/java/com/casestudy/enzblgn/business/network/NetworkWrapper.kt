package com.casestudy.enzblgn.business.network

import okhttp3.Headers
import okhttp3.Response
import okhttp3.ResponseBody


sealed class NetworkWrapper<out T> {
    /**
     * API Success response class from retrofit.
     *
     * [data] is optional. (There are responses without data)
     */
    data class Success<T>(val data: T, val rawResponse: Response) :
        NetworkWrapper<T>() {
        val statusCode: Int = rawResponse.code()
        val headers: Headers = rawResponse.headers()
    }

    /**
     * API Failure response class.
     *
     * ## API format error case.
     * API communication conventions do not match or applications need to handle errors.
     * e.g. internal server error.
     *
     * ## API Exception error case.
     * Gets called when an unexpected exception occurs while creating the request or processing the response in client.
     * e.g. network connection error.
     */
    sealed class Failure : NetworkWrapper<Nothing>() {
        data class Error(val rawResponse: Response) : Failure() {
            val statusCode: Int = rawResponse.code()
            val headers: Headers = rawResponse.headers()
            val errorBody: ResponseBody? = rawResponse.body()
        }

        data class Exception(val exception: Throwable) : Failure() {
            val message: String? = exception.localizedMessage
        }
    }
}