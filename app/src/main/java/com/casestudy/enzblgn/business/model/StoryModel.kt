package com.casestudy.enzblgn.business.model


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import java.util.*

@Parcelize
data class StoryModel(
    val category: String,
    val date: String,
    val image: ImageModel,
    val name: String,
    val text: String
) : Parcelable