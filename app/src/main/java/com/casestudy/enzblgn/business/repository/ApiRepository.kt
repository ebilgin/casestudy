package com.casestudy.enzblgn.business.repository

import com.casestudy.enzblgn.business.api.ApiInterface
import com.casestudy.enzblgn.business.data.ResponseData
import com.casestudy.enzblgn.common.extensions.toResource
import com.casestudy.enzblgn.presentation.data.Resource
import javax.inject.Inject


class ApiRepository @Inject constructor(private val api: ApiInterface) : BaseRepository() {

    suspend fun getResponse(): Resource<ResponseData> {
        val result = safeApiCal { api.getResponse() }
        return result.toResource()
    }
}