package com.casestudy.enzblgn.business.model

import com.casestudy.enzblgn.business.data.ResponseData
import com.casestudy.enzblgn.presentation.utils.DateUtils

class ResponseDataModel(val data: ResponseData) {
    var isBannerEnabled: Boolean = data.isBannerEnabled

    var meditations: List<MeditationModel> = data.meditations.map {
        MeditationModel(
            it.content,
            ImageModel(it.image.large, it.image.small),
            DateUtils.getDate(it.releaseDate),
            it.subtitle,
            it.title
        )
    }
    var stories: List<StoryModel> = data.stories.map {
        StoryModel(
            it.category,
            DateUtils.getDate(it.date),
            ImageModel(it.image.large, it.image.small),
            it.name,
            it.text
        )
    }
    var isBannerText: String = ""
}
