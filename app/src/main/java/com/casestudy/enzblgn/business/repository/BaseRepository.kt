package com.casestudy.enzblgn.business.repository

import com.casestudy.enzblgn.business.network.NetworkWrapper
import retrofit2.Response


abstract class BaseRepository {

    open suspend fun <T : Any> safeApiCal(call: suspend () -> Response<T>): NetworkWrapper<T> {
        return try {
            val response = call.invoke()
            if (response.isSuccessful) {
                NetworkWrapper.Success(response.body()!!, response.raw())
            } else {
                NetworkWrapper.Failure.Error(response.raw())
            }
        } catch (throwable: Exception) {
            NetworkWrapper.Failure.Exception(throwable)
        }
    }
}