package com.casestudy.enzblgn.business.data


import com.google.gson.annotations.SerializedName

data class StoryData(
    @SerializedName("category")
    val category: String,
    @SerializedName("date")
    val date: Long,
    @SerializedName("image")
    val image: ImageData,
    @SerializedName("name")
    val name: String,
    @SerializedName("text")
    val text: String
)