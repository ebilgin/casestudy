package com.casestudy.enzblgn.screen.home.adapters

import android.view.View
import coil.load
import coil.transform.RoundedCornersTransformation
import com.casestudy.enzblgn.R
import com.casestudy.enzblgn.business.model.StoryModel
import com.casestudy.enzblgn.databinding.ItemStoryBinding
import com.casestudy.enzblgn.presentation.adapter.BaseAdapter

class StoryAdapter(items: List<StoryModel> = emptyList()) : BaseAdapter<StoryModel>(
    R.layout.item_story, items,
) {
    override fun bind(
        itemView: View, item: StoryModel, position: Int, viewHolder: BaseViewHolderImp
    ) {
        val binding = ItemStoryBinding.bind(itemView)
        binding.imageStory.load(item.image.small){
            crossfade(true)
            transformations(RoundedCornersTransformation(20f))
        }
        binding.textName.text = item.name
        binding.textCategory.text = item.category
    }
}
