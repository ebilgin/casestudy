package com.casestudy.enzblgn.screen.home.viewmodels

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.casestudy.enzblgn.R
import com.casestudy.enzblgn.application.appstate.AppState
import com.casestudy.enzblgn.application.appstate.AppStateConstants
import com.casestudy.enzblgn.business.model.ResponseDataModel
import com.casestudy.enzblgn.business.repository.ApiRepository
import com.casestudy.enzblgn.common.extensions.mapResource
import com.casestudy.enzblgn.presentation.data.Resource
import com.casestudy.enzblgn.presentation.viewmodels.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val app: Application,
    private val appState: AppState,
    private val repository: ApiRepository
) : BaseViewModel() {

    fun getResponse(): LiveData<Resource<ResponseDataModel>> {
        val liveData = MutableLiveData<Resource<ResponseDataModel>>()
        liveData.postValue(Resource.Loading())
        viewModelScope.launch(Dispatchers.IO) {
            val resource = repository.getResponse()
            val result = resource.mapResource {
                ResponseDataModel(it).apply {
                    val userName = appState.getString(AppStateConstants.USER_NAME, "")
                    val bannerText = app.getString(R.string.home_banner, userName)
                    isBannerText = bannerText
                }
            }
            liveData.postValue(result)
        }
        return liveData
    }
}