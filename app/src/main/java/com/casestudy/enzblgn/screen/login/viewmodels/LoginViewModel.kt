package com.casestudy.enzblgn.screen.login.viewmodels

import androidx.lifecycle.MutableLiveData
import com.casestudy.enzblgn.R
import com.casestudy.enzblgn.application.appstate.AppState
import com.casestudy.enzblgn.application.appstate.AppStateConstants
import com.casestudy.enzblgn.presentation.utils.LiveDataValidator
import com.casestudy.enzblgn.presentation.utils.LiveDataValidatorResolver
import com.casestudy.enzblgn.presentation.viewmodels.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import java.util.regex.Pattern
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(private val appState: AppState) : BaseViewModel() {

    private val usernameLiveData = MutableLiveData<String>()
    val usernameValidator = LiveDataValidator(usernameLiveData).apply {
        //Whenever the condition of the predicate is true, the error message should be emitted
        addRule(R.string.login_username_rules) { it.isNullOrBlank() || it.length < 3 }
    }
    private val passwordLiveData = MutableLiveData<String>()
    val passwordValidator = LiveDataValidator(passwordLiveData).apply {
        //We can add multiple rules.
        //However the order in which they are added matters because the rules are checked one after the other
        addRule(R.string.login_password_rules) {
            it.isNullOrBlank() || it.length < 7 || !isLegalPassword(
                it
            )
        }
    }

    //We will use a mediator so we can update the error state of our form fields
    //and the enabled state of our login button as the form data changes
    val isLoginFormValidLiveData = MutableLiveData<Boolean>()

    fun login(username: String, password: String) {
        usernameLiveData.value = username
        passwordLiveData.value = password
        validateForm()
    }

    private fun validateForm() {
        val validators = listOf(usernameValidator, passwordValidator)
        val validatorResolver = LiveDataValidatorResolver(validators)
        val isValid = validatorResolver.isValid()

        if (isValid) {
            appState.putString(AppStateConstants.USER_NAME, usernameLiveData.value)
        }
        isLoginFormValidLiveData.postValue(isValid)
    }

    private fun isLegalPassword(pass: String): Boolean {
        val textPattern: Pattern = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).+$")
        return textPattern.matcher(pass).matches()
    }
}