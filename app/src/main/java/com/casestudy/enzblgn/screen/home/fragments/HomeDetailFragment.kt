package com.casestudy.enzblgn.screen.home.fragments

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.View
import androidx.fragment.app.viewModels
import coil.load
import com.casestudy.enzblgn.R
import com.casestudy.enzblgn.databinding.FragmentHomeDetailBinding
import com.casestudy.enzblgn.presentation.fragments.BaseFragment
import com.casestudy.enzblgn.presentation.utils.DateUtils
import com.casestudy.enzblgn.presentation.viewbinding.fragmentViewBinding
import com.casestudy.enzblgn.screen.home.viewmodels.HomeDetailViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class HomeDetailFragment : BaseFragment(R.layout.fragment_home_detail) {

    companion object {
        const val BUNDLE_MEDITATION_MODEL = "BUNDLE_MEDITATION_MODEL"
        const val BUNDLE_STORY_MODEL = "BUNDLE_STORY_MODEL"
    }

    override val binding by fragmentViewBinding(FragmentHomeDetailBinding::bind)

    private val viewModel: HomeDetailViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.init(arguments)

        viewModel.initMediaPlayer().observe(viewLifecycleOwner) {
            handleResource(it, {
                binding.togglePlayer.isEnabled = true
            }, loadingHandler = {
                binding.togglePlayer.isEnabled = false
            })
        }

        viewModel.meditation.observe(viewLifecycleOwner) {
            binding.toolbar.setTitle(R.string.home_detail_meditation)
            binding.imageDetail.load(it.image.large)
            binding.textDetailTitle.text = it.title
            binding.textDetail.text = it.content
            binding.textDate.text = it.releaseDate
        }

        viewModel.story.observe(viewLifecycleOwner) {
            binding.toolbar.setTitle(R.string.home_detail_story)
            binding.imageDetail.load(it.image.large)
            binding.textDetailTitle.text = it.name
            binding.textDetail.text = it.text
            binding.textDate.text = it.date
        }

        viewModel.alpha.observe(viewLifecycleOwner) {
            binding.imageBg.alpha = it
        }

        binding.textDetail.movementMethod = ScrollingMovementMethod()

        binding.toolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }

        binding.togglePlayer.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                viewModel.startPlayer()
            } else {
                viewModel.pausePlayer()
            }
        }
    }
}