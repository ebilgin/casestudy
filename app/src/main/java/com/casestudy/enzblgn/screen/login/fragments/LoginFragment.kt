package com.casestudy.enzblgn.screen.login.fragments

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.casestudy.enzblgn.R
import com.casestudy.enzblgn.databinding.FragmentLoginBinding
import com.casestudy.enzblgn.presentation.extensions.hideKeyboard
import com.casestudy.enzblgn.presentation.fragments.BaseFragment
import com.casestudy.enzblgn.presentation.viewbinding.fragmentViewBinding
import com.casestudy.enzblgn.screen.home.activities.HomeActivity
import com.casestudy.enzblgn.screen.login.viewmodels.LoginViewModel
import com.google.android.material.textfield.TextInputLayout.END_ICON_NONE
import com.google.android.material.textfield.TextInputLayout.END_ICON_PASSWORD_TOGGLE
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginFragment : BaseFragment(R.layout.fragment_login) {

    override val binding by fragmentViewBinding(FragmentLoginBinding::bind)

    private val viewModel: LoginViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonLogin.setOnClickListener {
            hideKeyboard()
            viewModel.login(
                binding.editUsername.text.toString(),
                binding.editPassword.text.toString()
            )
        }

        binding.inputUsername.editText?.onFocusChangeListener =
            View.OnFocusChangeListener { _, hasFocus ->
                if (hasFocus) {
                    binding.inputUsername.error = null
                    binding.inputUsername.isErrorEnabled = false
                }
            }

        binding.inputPassword.editText?.onFocusChangeListener =
            View.OnFocusChangeListener { _, hasFocus ->
                if (hasFocus) {
                    if (binding.inputPassword.endIconMode == END_ICON_NONE) {
                        binding.inputPassword.endIconMode = END_ICON_PASSWORD_TOGGLE
                        binding.inputPassword.error = null
                        binding.inputPassword.isErrorEnabled = false
                    }
                }
            }

        viewModel.usernameValidator.error.observe(viewLifecycleOwner) {
            binding.inputUsername.isErrorEnabled = true
            binding.inputUsername.error = getString(it)
        }

        viewModel.passwordValidator.error.observe(viewLifecycleOwner) {
            binding.inputPassword.isErrorEnabled = true
            binding.inputPassword.error = getString(it)
            binding.inputPassword.endIconMode = END_ICON_NONE
            binding.inputPassword.editText?.clearFocus()
        }

        viewModel.isLoginFormValidLiveData.value = false
        viewModel.isLoginFormValidLiveData.observe(viewLifecycleOwner) { isValid ->
            if (isValid) {
                val intent = Intent(requireActivity(), HomeActivity::class.java)
                startActivity(intent)
            }
        }
    }
}