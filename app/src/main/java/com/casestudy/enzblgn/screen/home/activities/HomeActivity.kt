package com.casestudy.enzblgn.screen.home.activities

import com.casestudy.enzblgn.databinding.ActivityHomeBinding
import com.casestudy.enzblgn.presentation.activities.BaseActivity
import com.casestudy.enzblgn.presentation.viewbinding.activityViewBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeActivity : BaseActivity() {

    override val binding by activityViewBinding(ActivityHomeBinding::inflate)

}