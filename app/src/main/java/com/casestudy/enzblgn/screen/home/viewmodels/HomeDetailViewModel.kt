package com.casestudy.enzblgn.screen.home.viewmodels

import android.app.Application
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.casestudy.enzblgn.BuildConfig
import com.casestudy.enzblgn.business.model.MeditationModel
import com.casestudy.enzblgn.business.model.StoryModel
import com.casestudy.enzblgn.presentation.data.Resource
import com.casestudy.enzblgn.presentation.viewmodels.BaseViewModel
import com.casestudy.enzblgn.screen.home.fragments.HomeDetailFragment
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeDetailViewModel @Inject constructor(private val app: Application) : BaseViewModel() {

    private val _meditation = MutableLiveData<MeditationModel>()
    val meditation: LiveData<MeditationModel> = _meditation

    private val _story = MutableLiveData<StoryModel>()
    val story: LiveData<StoryModel> = _story

    private val _alpha = MutableLiveData<Float>()
    val alpha: LiveData<Float> = _alpha

    private var mediaPlayer: MediaPlayer? = null

    fun init(arguments: Bundle?) {
        arguments?.apply {
            val meditation: MeditationModel? =
                getParcelable(HomeDetailFragment.BUNDLE_MEDITATION_MODEL)
            val story: StoryModel? = getParcelable(HomeDetailFragment.BUNDLE_STORY_MODEL)
            meditation?.let {
                _meditation.postValue(it)
            }
            story?.let {
                _story.postValue(it)
            }
        }
    }

    fun initMediaPlayer(): LiveData<Resource<Boolean>> {
        val liveData = MutableLiveData<Resource<Boolean>>()
        liveData.postValue(Resource.Loading())
        viewModelScope.launch(Dispatchers.IO) {
            val audioUri: Uri = Uri.parse(BuildConfig.MEDIA_URL)
            mediaPlayer = MediaPlayer().apply {
                setAudioAttributes(
                    AudioAttributes.Builder()
                        .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                        .setUsage(AudioAttributes.USAGE_MEDIA)
                        .build()
                )
                setDataSource(app.applicationContext, audioUri)
                prepare()
                liveData.postValue(Resource.Success(true))
            }
        }
        return liveData
    }

    fun startPlayer() {
        mediaPlayer?.start()
        _alpha.postValue(0.9f)
    }

    fun pausePlayer() {
        mediaPlayer?.pause()
        _alpha.postValue(0.7f)
    }

    override fun onCleared() {
        super.onCleared()
        mediaPlayer?.stop()
    }
}