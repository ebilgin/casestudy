package com.casestudy.enzblgn.screen.login.activities

import com.casestudy.enzblgn.databinding.ActivityLoginBinding
import com.casestudy.enzblgn.presentation.activities.BaseActivity
import com.casestudy.enzblgn.presentation.viewbinding.activityViewBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginActivity : BaseActivity() {

    override val binding by activityViewBinding(ActivityLoginBinding::inflate)

}