package com.casestudy.enzblgn.screen.home.adapters

import android.view.View
import com.casestudy.enzblgn.R
import com.casestudy.enzblgn.business.model.ResponseDataModel
import com.casestudy.enzblgn.databinding.ItemBannerSectionBinding
import com.casestudy.enzblgn.presentation.adapter.BaseAdapter

class BannerSectionAdapter(
    items: List<ResponseDataModel> = emptyList(),
) : BaseAdapter<ResponseDataModel>(
    R.layout.item_banner_section,
    items,
) {
    override fun bind(
        itemView: View,
        item: ResponseDataModel,
        position: Int,
        viewHolder: BaseViewHolderImp
    ) {

        val binding = ItemBannerSectionBinding.bind(itemView)
        binding.textSectionTitle.text = item.isBannerText
    }
}