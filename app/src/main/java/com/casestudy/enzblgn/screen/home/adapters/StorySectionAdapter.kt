package com.casestudy.enzblgn.screen.home.adapters

import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.casestudy.enzblgn.R
import com.casestudy.enzblgn.business.model.MeditationModel
import com.casestudy.enzblgn.business.model.ResponseDataModel
import com.casestudy.enzblgn.business.model.StoryModel
import com.casestudy.enzblgn.databinding.ItemStorySectionBinding
import com.casestudy.enzblgn.presentation.adapter.BaseAdapter

class StorySectionAdapter(items: List<ResponseDataModel> = emptyList()) :
    BaseAdapter<ResponseDataModel>(R.layout.item_story_section, items) {

    private val viewPool = RecyclerView.RecycledViewPool()
    private var callbackNested: OnItemClickListener<StoryModel>? = null

    override fun bind(
        itemView: View,
        item: ResponseDataModel,
        position: Int,
        viewHolder: BaseViewHolderImp
    ) {

        val binding = ItemStorySectionBinding.bind(itemView)

        val layoutManager = GridLayoutManager(itemView.context, 2)

        layoutManager.initialPrefetchItemCount = 4

        binding.listStory.run {
            this.setRecycledViewPool(viewPool)
            this.layoutManager = layoutManager


            val storyAdapter = StoryAdapter(item.stories)
            storyAdapter.setOnItemClickListener(object : OnItemClickListener<StoryModel> {
                override fun onItemClick(item: StoryModel) {
                    callbackNested?.onItemClick(item)
                }
            })
            this.adapter = storyAdapter
        }
    }

    fun setOnNestedItemClickListener(callback: OnItemClickListener<StoryModel>) {
        this.callbackNested = callback
    }
}