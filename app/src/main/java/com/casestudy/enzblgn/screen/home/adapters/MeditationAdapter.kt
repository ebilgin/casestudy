package com.casestudy.enzblgn.screen.home.adapters

import android.view.View
import coil.load
import coil.transform.RoundedCornersTransformation
import com.casestudy.enzblgn.R
import com.casestudy.enzblgn.business.model.MeditationModel
import com.casestudy.enzblgn.databinding.ItemMeditationBinding
import com.casestudy.enzblgn.presentation.adapter.BaseAdapter

class MeditationAdapter(items: List<MeditationModel> = emptyList()) : BaseAdapter<MeditationModel>(
    R.layout.item_meditation, items,
) {
    override fun bind(
        itemView: View, item: MeditationModel, position: Int, viewHolder: BaseViewHolderImp
    ) {
        val binding = ItemMeditationBinding.bind(itemView)
        binding.imageMeditation.load(item.image.small) {
            crossfade(true)
            transformations(RoundedCornersTransformation(20f))
        }
        binding.textTitle.text = item.title
        binding.textSubtitle.text = item.subtitle
    }
}
