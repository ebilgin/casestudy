package com.casestudy.enzblgn.screen.home.adapters

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.casestudy.enzblgn.R
import com.casestudy.enzblgn.business.model.MeditationModel
import com.casestudy.enzblgn.business.model.ResponseDataModel
import com.casestudy.enzblgn.databinding.ItemMeditationSectionBinding
import com.casestudy.enzblgn.presentation.adapter.BaseAdapter

class MeditationSectionAdapter(items: List<ResponseDataModel> = emptyList()) :
    BaseAdapter<ResponseDataModel>(R.layout.item_meditation_section, items) {

    private val viewPool = RecyclerView.RecycledViewPool()
    private var callbackNested: OnItemClickListener<MeditationModel>? = null

    override fun bind(
        itemView: View,
        item: ResponseDataModel,
        position: Int,
        viewHolder: BaseViewHolderImp
    ) {

        val binding = ItemMeditationSectionBinding.bind(itemView)

        val layoutManager =
            LinearLayoutManager(itemView.context, LinearLayoutManager.HORIZONTAL, false)

        layoutManager.initialPrefetchItemCount = 4

        binding.listMeditation.run {
            this.setRecycledViewPool(viewPool)
            this.layoutManager = layoutManager
            val meditationAdapter = MeditationAdapter(item.meditations)
            meditationAdapter.setOnItemClickListener(object : OnItemClickListener<MeditationModel> {
                override fun onItemClick(item: MeditationModel) {
                    callbackNested?.onItemClick(item)
                }
            })
            this.adapter = meditationAdapter

        }
    }

    fun setOnNestedItemClickListener(callback: OnItemClickListener<MeditationModel>) {
        this.callbackNested = callback
    }
}