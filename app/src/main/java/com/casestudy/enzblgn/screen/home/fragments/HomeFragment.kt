package com.casestudy.enzblgn.screen.home.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.commit
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.ConcatAdapter
import com.casestudy.enzblgn.R
import com.casestudy.enzblgn.business.model.MeditationModel
import com.casestudy.enzblgn.business.model.ResponseDataModel
import com.casestudy.enzblgn.business.model.StoryModel
import com.casestudy.enzblgn.databinding.FragmentHomeBinding
import com.casestudy.enzblgn.presentation.adapter.BaseAdapter
import com.casestudy.enzblgn.presentation.extensions.enforceSingleScrollDirection
import com.casestudy.enzblgn.presentation.fragments.BaseFragment
import com.casestudy.enzblgn.presentation.viewbinding.fragmentViewBinding
import com.casestudy.enzblgn.screen.home.adapters.BannerSectionAdapter
import com.casestudy.enzblgn.screen.home.adapters.MeditationSectionAdapter
import com.casestudy.enzblgn.screen.home.adapters.StorySectionAdapter
import com.casestudy.enzblgn.screen.home.viewmodels.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : BaseFragment(R.layout.fragment_home) {

    override val binding by fragmentViewBinding(FragmentHomeBinding::bind)

    private val viewModel: HomeViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getResponse().observe(viewLifecycleOwner) { res ->
            handleResource(res, {
                indicatorPresenter.hide()
                val data = listOf(it.data)
                val concatAdapter = ConcatAdapter()
                val meditationAdapter = getMeditationAdapter(data)
                val storyAdapter = getStoryAdapter(data)
                concatAdapter.addAdapter(meditationAdapter)
                if (it.data.isBannerEnabled) {
                    val bannerAdapter = BannerSectionAdapter(data)
                    concatAdapter.addAdapter(bannerAdapter)
                }
                concatAdapter.addAdapter(storyAdapter)
                binding.listHome.run {
                    adapter = concatAdapter
                    enforceSingleScrollDirection()
                }
            })
        }
    }

    private fun getStoryAdapter(data: List<ResponseDataModel>): StorySectionAdapter {
        val storyAdapter = StorySectionAdapter(data)
        storyAdapter.setOnNestedItemClickListener(object :
            BaseAdapter.OnItemClickListener<StoryModel> {
            override fun onItemClick(item: StoryModel) {
                val bundle = Bundle()
                bundle.putParcelable(HomeDetailFragment.BUNDLE_STORY_MODEL, item)
                openDetail(bundle)
            }
        })
        return storyAdapter
    }

    private fun getMeditationAdapter(data: List<ResponseDataModel>): MeditationSectionAdapter {
        val meditationAdapter = MeditationSectionAdapter(data)
        meditationAdapter.setOnNestedItemClickListener(object :
            BaseAdapter.OnItemClickListener<MeditationModel> {
            override fun onItemClick(item: MeditationModel) {
                val bundle = Bundle()
                bundle.putParcelable(HomeDetailFragment.BUNDLE_MEDITATION_MODEL, item)
                openDetail(bundle)
            }
        })
        return meditationAdapter
    }

    private fun openDetail(bundle: Bundle) {
        requireActivity().supportFragmentManager.commit {
            addToBackStack(HomeDetailFragment::class.java.simpleName)
            replace(R.id.fragmentContainerView, HomeDetailFragment::class.java, bundle)
        }
    }
}