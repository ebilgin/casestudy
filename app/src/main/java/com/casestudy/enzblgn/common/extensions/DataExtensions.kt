package com.casestudy.enzblgn.common.extensions

import com.casestudy.enzblgn.R
import com.casestudy.enzblgn.business.network.NetworkWrapper
import com.casestudy.enzblgn.presentation.data.Resource
import com.casestudy.enzblgn.presentation.data.ResourceDefaultError
import java.net.SocketTimeoutException
import java.net.UnknownHostException


typealias ResourceSuccessHandler<T> = ((resource: Resource.Success<T>) -> Unit)
typealias ResourceErrorHandler<T> = ((resource: Resource.Error<T>) -> Boolean)
typealias ResourceLoadingHandler<T> = ((resource: Resource.Loading<T>) -> Unit)

typealias SuccessHandler<T, Y> = (value: NetworkWrapper.Success<T>) -> Resource<Y>
typealias ErrorHandler<Y> = (value: NetworkWrapper.Failure.Error) -> Resource<Y>
typealias ExceptionHandler<Y> = (value: NetworkWrapper.Failure.Exception) -> Resource<Y>

fun <T, Y> NetworkWrapper<T>.toResource(
    successHandler: SuccessHandler<T, Y>? = null,
    errorHandler: ErrorHandler<Y>? = null,
    exceptionHandler: ExceptionHandler<Y>? = null,
): Resource<Y> {
    return when (this) {
        is NetworkWrapper.Success -> {
            if (successHandler == null) {
                @Suppress("UNCHECKED_CAST")
                Resource.Success(this.data as Y)
            } else {
                successHandler(this)
            }
        }
        is NetworkWrapper.Failure.Error -> {
            if (errorHandler == null) {
                Resource.Error(
                    messageResID = R.string.common_technical_error_message,
                    error = ResourceDefaultError.HTTP_ERROR
                )
            } else {
                errorHandler(this)
            }
        }
        is NetworkWrapper.Failure.Exception -> {
            if (exceptionHandler == null) {
                when (this.exception) {
                    is SocketTimeoutException -> {
                        Resource.Error(
                            messageResID = R.string.common_connection_exception_message,
                            error = ResourceDefaultError.TIMEOUT_ERROR
                        )
                    }
                    is UnknownHostException -> {
                        Resource.Error(
                            messageResID = R.string.common_connection_exception_message,
                            error = ResourceDefaultError.NOT_CONNECTION_ERROR
                        )
                    }
                    else -> {
                        Resource.Error(
                            messageResID = R.string.common_technical_error_message,
                            error = ResourceDefaultError.GENERAL_ERROR
                        )
                    }
                }
            } else {
                exceptionHandler(this)
            }
        }
    }
}

inline fun <T, Y> Resource<T>.mapResource(crossinline transform: (T) -> Y): Resource<Y> {
    return when (this) {
        is Resource.Success<T> -> Resource.Success(transform(data))
        is Resource.Loading<T> -> Resource.Loading(
            data = data?.let { transform(it) },
            loadingMessageResId = loadingMessageResId
        )
        is Resource.Error<T> -> Resource.Error(
            data = data?.let { transform(it) },
            messageResID = messageResID,
            error = error
        )
    }
}

